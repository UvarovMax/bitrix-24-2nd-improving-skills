<?php

namespace app\controllers;


use Yii;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;
use yii\helpers\FileHelper;
use \yii\base\ExitException;

class DealTransferController extends Controller
{
    public $enableCsrfValidation = false;
    protected static $bitrix24;
    const CACHE_KEY = 'transfer_id';

    public function init()
    {
        parent::init();

        if (!Yii::$app->bitrix24->isAdmin()) {
            throw new UnauthorizedHttpException('Доступ разрешен только администраторам портала');
        }

        self::$bitrix24 = \Yii::$app->bitrix24->admin();

        $uploadsPath = Yii::getAlias("@webroot/uploads");

        if (!is_dir($uploadsPath)) {
            mkdir($uploadsPath, 0777, true);
        }
    }

    public function actionIndex()
    {
        $flashParams = \Yii::$app->session->getFlash('params');

        $deals = self::$bitrix24->call("crm.deal.list", [
            'select' => ['ID'],
            'order' => ['ID' => 'ASC'],
        ]);
        if(self::$bitrix24->hasBatchCalls())
            $flashParams["deal-add-result"] = "Есть незаписанные данные!";

        return $this->render("index",
            [
                "dealsCount" => $deals["total"],
                "dealAddResult" => $flashParams["deal-add-result"] ?? "",
                "dealGetResult" => $flashParams["deal-get-result"] ?? ""
            ]
        );
    }

    public function actionAdd()
    {
        $dealsCount = (int)Yii::$app->request->post("template-deals");

        if ($dealsCount <= 0) {
            return $this->redirect('index');
        }

        if(self::$bitrix24->hasBatchCalls()) {
            Yii::$app->session->setFlash('params', ["deal-add-result" => "Есть незаписанные данные!"]);
            Yii::$app->end();
            return $this->redirect("index");
        }

        $dealParams = [
            "TITLE" => "",
            "TYPE_ID" => "GOODS",
            "STAGE_ID" => "NEW",
            "COMPANY_ID" => 3,
            "CONTACT_ID" => 3,
            "OPENED" => "Y",
            "ASSIGNED_BY_ID" => 1,
            "PROBABILITY" => 30,
            "CURRENCY_ID" => "RUB",
            "OPPORTUNITY" => "1000",
            "CATEGORY_ID" => 5,
        ];

        $arDeal = [];
        $connection = \Yii::$app->db;
        Yii::$app->cache->delete(self::CACHE_KEY);

        for ($step = 0; $step < $dealsCount; $step++) {

            $dealParams["TITLE"] = "Плановая продажа $step";

            self::$bitrix24->addBatchCall("crm.deal.add", [
                "fields" => $dealParams,
                "type" => "batch.add"
            ],
                function ($result) use (&$arDeal, $dealParams, $connection) {
                    $arDeal = $result['result'];

                    array_unshift($arDeal, $result['result']);
                    Yii::$app->cache->set(self::CACHE_KEY, $arDeal);

                    $connection->createCommand()->insert("deal",
                        [
                            "DEAL_ID" => $result["result"],
                            "TITLE" => "{$dealParams["TITLE"]}",
                            "STAGE_ID" => "{$dealParams["STAGE_ID"]}",
                        ]
                    )->execute();
                }
            );

            if ($step % 50 == 0) {
                self::$bitrix24->processBatchCalls();
                usleep(500);
            }
        }
        usleep(500);
        self::$bitrix24->processBatchCalls();

        Yii::$app->session->setFlash('params', ["deal-add-result" => "Операция импорта выполнена успешно!"]);
        return $this->redirect("index");
    }

    public function actionGet()
    {
        $dealsArray = [];
        $bitrix24 = self::$bitrix24;

        $uploadDirectory = Yii::getAlias('@webroot/uploads');
        $filename = date("Y-m-d-H-i-s") . '-deals.csv';

        FileHelper::createDirectory($uploadDirectory);
        $filePath = $uploadDirectory . DIRECTORY_SEPARATOR . $filename;


        $bitrix24->addBatchCall('crm.deal.list',
            [
                'select' => ['ID', 'TITLE', 'TYPE_ID', 'STAGE_ID', 'CURRENCY_ID', 'OPPORTUNITY', 'DATE_MODIFY'],
                'order' => ['ID' => 'ASC']
            ],
            function ($result) use ($bitrix24, &$dealsArray) {
                // save first page
                foreach ($result['result'] as $deal) {
                    $dealsArray[$deal['ID']] = $deal;
                }
                // add calls for subsequent pages
                for ($i = $result['next']; $i < $result['total']; $i += $result['next']) {
                    $bitrix24->addBatchCall('crm.deal.list',
                        [
                            'start' => $i,
                            'select' => ['ID', 'TITLE', 'TYPE_ID', 'STAGE_ID', 'CURRENCY_ID', 'OPPORTUNITY', 'DATE_MODIFY'],
                            'order' => ['ID' => 'ASC']
                        ],
                        function ($result) use (&$dealsArray) {
                            // save subsequent page
                            foreach ($result['result'] as $contact) {
                                $dealsArray[$contact['ID']] = $contact;
                            }
                        });
                }
            });
        self::$bitrix24->processBatchCalls();

        $handle = fopen($filePath, 'w');
        $csvHeader = array_keys($dealsArray[array_key_first($dealsArray)]);
        fputcsv($handle, $csvHeader);

        foreach ($dealsArray as $deal) {
            fputcsv($handle, $deal);
        }

        fclose($handle);

        Yii::$app->session->setFlash('params', ["deal-get-result" => "Операция экспорта выполнена успешно!"]);
        Yii::$app->response->sendFile($filePath, null, [
            'mimeType' => 'text/csv',
            'inline' => false,
        ])->send();
    }
}