<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

/**
 * Class DealEventController
 *
 * Предназначен для регистрации и реализации логики:
 * <br> • событий сделок CRM - onCrmDealAdd
 * <br> • webhook - любое изменение событий сделок CRM
 * <br> • встраивание в карточку сделок - CRM_DEAL_DETAIL_TAB
 */
class DealEventController extends Controller
{
    public $enableCsrfValidation = false;
    protected static $bitrix24;
    const CACHE_KEY = 'transfer_id';

    public function init()
    {
        parent::init();

        self::$bitrix24 = \Yii::$app->bitrix24->admin();
    }

    public function actionIndex()
    {
        $binds = $this->actionCheckBind();
        return $this->render("index",
            [
                "eventBind" => $binds["event"],
                "placementBind" => $binds["placement"]
            ]
        );
    }

    public function actionEventBind()
    {
        self::$bitrix24->call(
            "event.bind",
            [
                'event' => 'onCrmDealAdd',
                'handler' => Yii::$app->params["app"]["event_handler"] . "on-deal-add",
            ]
        );
        $this->redirect("index");
    }

    public function actionEventUnbind()
    {
        self::$bitrix24->call(
            "event.unbind",
            [
                'event' => 'onCrmDealAdd',
                'handler' => Yii::$app->params["app"]["event_handler"] . "on-deal-add",
            ]
        );
        $this->redirect("index");
    }

    public function actionPlacementBind()
    {
        self::$bitrix24->call(
            "placement.bind",
            [
                "PLACEMENT" => "CRM_DEAL_DETAIL_TAB",
                "HANDLER" => Yii::$app->params["app"]["event_handler"] . "deal-info",
                'TITLE' => 'Встраиваемое сведение карточки',
            ]
        );
        $this->redirect("index");
    }

    public function actionPlacementUnbind()
    {
        self::$bitrix24->call(
            "placement.unbind",
            [
                'PLACEMENT' => 'CRM_DEAL_DETAIL_TAB',
            ]
        );
        $this->redirect("index");
    }

    public function actionCheckBind()
    {
        $resultEvent = self::$bitrix24->call("event.get");
        $resultPlacement = self::$bitrix24->call("placement.get");

        $resultEvent = $resultEvent["result"][0]["event"] ?? "";
        $resultPlacement = $resultPlacement["result"][0]["placement"] ?? "";

        return ["event" => $resultEvent, "placement" => $resultPlacement];
    }

    public function actionOnDealAdd()
    {
        if ($_REQUEST["event"] != "ONCRMDEALADD") {
//            Yii::info("Обработчик настроен на другую операцию", "DealEvent");
            return;
        }

        if (in_array($_REQUEST["data"]["FIELDS"]["ID"], Yii::$app->cache->get(self::CACHE_KEY))) {
//            Yii::info("Данные из транзакции скипаем", "DealEvent");
            return;
        }

        $dealID = $_REQUEST["data"]["FIELDS"]["ID"];
        $dealData = self::$bitrix24->call("crm.deal.get", ["ID" => $dealID]);

        if (!empty($dealData["result"])) {
            $connection = \Yii::$app->db;
            $connection->createCommand()->insert("deal",
                [
                    "DEAL_ID" => $dealID,
                    "TITLE" => "{$dealData["result"]["TITLE"]}",
                    "STAGE_ID" => "{$dealData["result"]["STAGE_ID"]}",
                    "DATE_CREATE" => "{$dealData["result"]["DATE_CREATE"]}",
                ]
            )->execute();
        }
    }

    public function actionDealUpdate()
    {
        $dealID = (int)Yii::$app->request->get("id");

        if ($dealID !== 0) {
            $dealData = self::$bitrix24->call("crm.deal.get", ["ID" => $dealID]);
            $connection = \Yii::$app->db;
            $connection->createCommand()->update("deal",
                [
                    "DATE_MODIFY" => "{$dealData["result"]["DATE_MODIFY"]}"
                ],
                "DEAL_ID = $dealID"
            )->execute();
        }
        return;
    }

    public function actionDealInfo()
    {
        $this->layout = "placement";
        $dealDataBitrix = [];
        $dealDataDB = [];

        if ($_REQUEST["PLACEMENT"] == "CRM_DEAL_DETAIL_TAB" && !empty($_REQUEST["PLACEMENT_OPTIONS"])) {

            $dealID = json_decode($_REQUEST["PLACEMENT_OPTIONS"], true)["ID"];

            if (!empty($dealID)) {
                // На сдачу
                // реализовать проверку на существование сделки
                $dealDataBitrix = self::$bitrix24->call(
                    "crm.deal.get",
                    ["ID" => $dealID]
                );

                $dealDataDB = (new \yii\db\Query())
                    ->select("*")
                    ->from("deal")
                    ->where(["ID" => $dealID])
                    ->limit(1)
                    ->one();
            }
        }

        return $this->render('card-info',
            [
                "dealDataBitrix" => $dealDataBitrix["result"],
                "dealDataDB" => $dealDataDB,
            ]
        );
    }
}