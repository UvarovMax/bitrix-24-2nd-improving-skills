<?php

use yii\db\Migration;

/**
 * Class m240217_174612_deal
 */
class m240217_174612_deal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('deal',
        [
            "ID" => $this->primaryKey(),
            "TITLE" => $this->string(),
            "STAGE_ID" => $this->bigInteger(),
            "DATE_CREATE" => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('deal');
        //        echo "m240217_174612_deal cannot be reverted.\n";

        //        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240217_174612_deal cannot be reverted.\n";

        return false;
    }
    */
}
