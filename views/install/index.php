<?php
/** @var bool $hasCredentials */
/** @var string $credentialsStatus */
/** @var array $installStatus */

/** @var \yii\web\View $this */

$this->title = 'Установка приложения';

?>

<div class="row">
    <div class="col-xs-3">
        <div class="thumbnail">
            <div class="caption">
                <h3>Установка приложения</h3>
                <hr>
                <p><?= $installStatus['message'] ?></p>
                <hr>
                <?php if (!$installStatus['success']): ?>
                    <p><a href="/web/install/finish" class="btn btn-primary" role="button">Установить</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="thumbnail">
            <div class="caption">
                <h3>Доступы администратора</h3>
                <hr>
                <p><?= $credentialsStatus['message'] ?></p>
                <hr>
                <p><a href="/web/install/set-admin-credentials" class="btn btn-primary" role="button">Обновить</a>
                </p>
            </div>
        </div>
    </div>
</div>