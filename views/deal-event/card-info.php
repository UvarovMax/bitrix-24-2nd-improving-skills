<?php
/** @var array $dealDataBitrix */
/** @var array $dealDataDB */
/** @var \yii\web\View $this */

$this->title = 'Встраиваемое сведение карточки';
?>

<h3>Сведения из Битрикс24</h3>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Значение</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($dealDataBitrix as $fieldName => $value) {
        echo "<tr>
                <td>$fieldName</td>
                <td>$value</td>
            </tr>";
    }
    ?>
    </tbody>
</table>
<h3>Сведения из БД</h3>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Значение</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($dealDataDB as $fieldName => $value) {
        echo "<tr>
                <td>$fieldName</td>
                <td>$value</td>
            </tr>";
    }
    ?>
    </tbody>
</table>