<?php
/** @var string $eventBind */
/** @var string $placementBind */
/** @var \yii\web\View $this */

$this->title = 'События и встраивания в Сделки';
?>

<div class="row">
    <div class="col-xs-3 p-2">
        <div class="thumbnail">
            <div class="caption">
                <h3>Зарегистрированные события</h3>
                <hr>
                <p><?= $eventBind ?></p>
                <hr>
                <?php if (empty($eventBind)): ?>
                    <p><a href="/web/deal-event/event-bind" class="btn btn-primary" role="button">Зарегистрировать</a></p>
                <?php else: ?>
                    <p><a href="/web/deal-event/event-unbind" class="btn btn-primary" role="button">Отменить</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-3 p-2">
        <div class="thumbnail">
            <div class="caption">
                <h3>Зарегистрированные встраивания</h3>
                <hr>
                <p><?= $placementBind ?></p>
                <hr>
                <?php if (empty($placementBind)): ?>
                    <p><a href="/web/deal-event/placement-bind" class="btn btn-primary" role="button">Зарегистрировать</a></p>
                <?php else: ?>
                    <p><a href="/web/deal-event/placement-unbind" class="btn btn-primary" role="button">Отменить</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>