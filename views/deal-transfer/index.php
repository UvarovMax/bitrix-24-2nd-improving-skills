<?php
/** @var integer $dealsCount */
/** @var integer $dealAddResult */
/** @var integer $dealGetResult */
/** @var array $var_dump */
/** @var \yii\web\View $this */

$this->title = 'Импорт/экспорт сделок';
?>
<h3>Импорт/экспорт сделок</h3>
<div class="row">
    <form action="/web/deal-transfer/add" method="post" class="col-xl-4 flex-column">
        <h4>Импорт</h4>
        <div class="form-group input-group mb-3">
            <input required type="number" min="1" max="5000" step="1" name="template-deals" class="form-control"
                   placeholder="Количество сделок"
                   aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Сгенерировать</button>
            </div>
        </div>
        <p><?= $dealAddResult ?></p>
    </form>


    <form action="/web/deal-transfer/get" method="post" class="col-xl-4 flex-column">
        <h4>Экспорт</h4>
        <p>Сделок в системе: <?= $dealsCount ?></p>
        <?php if ($dealsCount > 0): ?>
<!--            <button class="btn btn-primary" type="submit">Экспортировать в csv</button>-->
            <a href="/web/deal-transfer/get" class="btn btn-primary">Экспортировать в csv</a>
        <?php endif; ?>
        <p><?= $dealGetResult ?></p>
    </form>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        function handleFormSubmit() {
            document.querySelectorAll(".btn").forEach((button) => {
                button.style.pointerEvents = "none";
                button.classList.add("btn-secondary");
            });
        }

        document.querySelectorAll("form").forEach((form) => {
            form.addEventListener('submit', handleFormSubmit);
        });

    });
</script>
